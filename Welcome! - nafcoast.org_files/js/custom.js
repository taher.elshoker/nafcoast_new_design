$(function() {
    $('.scroll-banner').on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 800, 'linear');
    });


    $(window).scroll(function(){
      
       if($(this).scrollTop()>=2)
       {
        document.getElementById("custom-nav").classList.add("bg-scroll");
      }
    else{
     
        document.getElementById("custom-nav").classList.remove("bg-scroll");

      }
    });

    $('.navbar-toggle').on('click', function(e) {
      e.preventDefault();
      document.getElementById("custom-nav").classList.add("bg-scroll");});


  });  